#!/bin/sh

set -eu

mkdir -p "${HOME}/.ssh"
cat "$PRIVATE_KEY" > "${HOME}/.ssh/id_ed25519"
rm -f "$PRIVATE_KEY" || true
chmod 0600 "${HOME}/.ssh/id_ed25519"
git config --global user.name "Charts uploader"
git config --global user.email "uploader@charts.kube-ops.io"

cd "$(mktemp -d)"
git clone --single-branch --branch master --depth 1 'git@gitlab.com:kube-ops/helm/repository.git' repository
cd repository

chartName="$CHART_NAME"
chartVersion="$CHART_VERSION"
projectPath="$PROJECT_PATH"

rm -rf chart
git clone --single-branch --branch "$chartVersion" "git@gitlab.com:$projectPath" chart
rm -rf chart/.git
rm -f chart/*.tgz

if [ ! -r chart/.helmignore ]; then
	wget -O chart/.helmignore https://gitlab.com/kube-ops/helm/template/-/raw/master/.helmignore
fi

cd chart
helm dep up
helm package .
mv -vf "${chartName}-${chartVersion}.tgz" "../public/${chartName}-${chartVersion}.tgz"
cd ..
rm -rf chart
git status
git add -A
git commit -m "Upload New File"
git push -u origin master

cd ..
rm -rf repository "${HOME}/.ssh"
